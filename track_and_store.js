// tracks mouse movements, and save them in a json file

// loads
var mouse = require('osx-mouse')();
const exec = require('child_process').exec;
var player = require('play-sound')(opts = {});
var caption = require('caption');
var jsonfile = require('jsonfile');
const fs = require('fs');

// params
var separateMoveThreshold = 2000;
var buckets = [20]; // max 1000, 50 length, 20 pieces of buckets TODO

// vars
var lastMove = 0;
var moves = [];

mouse.on('move', function(x, y) {
	checkOnMouse();
    lastMove = new Date().getTime();
    moves.push([x,y]);
});

function checkOnMouse() {
    if (new Date().getTime() - lastMove > separateMoveThreshold) {
    	if (moves.length > 10)
    		saveMoves(moves);
    	moves = [];
    }
}
setInterval(checkOnMouse, 100);

function saveMoves(moves) {
    var t = Math.floor(new Date().getTime() / (1000 * 3600)); // every hour
    var file = 'mouse_tracks/' + t + '.json';
    fs.stat(file, function(err, stats) {
    	if (err) { // create and fill file
    		framed_moves = [moves]; // so we can append later
    		jsonfile.writeFile(file, framed_moves);
    		console.log(file + ' created');
    	}
    	else { // load, append, and save file
    		jsonfile.readFile(file, function(err, prev_moves) {
    			prev_moves.push(moves);
    			jsonfile.writeFile(file, prev_moves);
    			console.log(file + ' updated');
    		});
    	}
    });
}
