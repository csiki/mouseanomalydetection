#!/usr/bin/env python

import importlib
import sys
import csv
import datetime
import os, sys
import csv
import pickle

from nupic.data.inference_shifter import InferenceShifter
from nupic.frameworks.opf.modelfactory import ModelFactory
from nupic.frameworks.opf.model import Model
from nupic.algorithms import anomaly_likelihood

# trains a htm model on the merged dataset 

INPUT_FILE = 'v.csv'  # mouse tracking information to train on
OUPUT_FILE = 'v_out.cs'  # anomaly scores, likelihood values are saved in this file for later inspection if needed

# name of the swarm model - the file {MODEL_PARAMS_DIR}/model_params/{MODEL_PARAMS_NAME}_model_params.py gets imported
MODEL_PARAMS_DIR = 'swarm'
MODEL_PARAMS_NAME = 'small_v'

DATE_FORMAT = '%m/%d/%y %H:%M'
LH_FILE = 'anomalyLikelihood.pickle'
MODEL_PATH = 'htm/model'


def createModel(modelParams):
    model = ModelFactory.create(modelParams)
    model.enableInference({'predictedField': 'vx'})
    return model


def getModelParamsFromName(modelParamsName):
    importName = 'model_params.%s_model_params' % (
        modelParamsName.replace(' ', '_').replace('-', '_')
    )
    print 'Importing model params from %s' % importName
    try:
        sys.path.insert(0, os.path.join(sys.path[0], MODEL_PARAMS_DIR))
        importedModelParams = importlib.import_module(importName).MODEL_PARAMS
    except ImportError:
        raise Exception('No model params exist for %s. Run swarm first!' % modelParamsName)
    return importedModelParams


def runIoThroughNupic(inputData, model, modelParamsName, loadPrevModel):
    inputFile = open(inputData, 'rb')
    csvReader = csv.reader(inputFile)

    # skip header rows
    csvReader.next()
    csvReader.next()
    csvReader.next()

    counter = 0
    outf = open('small_v_out.csv', 'wt')
    writer = csv.writer(outf)
    writer.writerow(('seq', 'timestamp', 'vx' ,'vy', 'prediction', 'score', 'likelihood', 'log(likelihood)'))

    initial_seq = 0

    if loadPrevModel and os.path.exists(LH_FILE):
        with open(LH_FILE, 'rb') as lhFile:
            initial_seq, anomalyLikelihood = pickle.load(lhFile)
    else:
        anomalyLikelihood = anomaly_likelihood.AnomalyLikelihood()

    for row in csvReader:

        if int(row[0]) <= initial_seq:
            continue

        counter += 1
        if counter % 1000 == 0:
            print 'Read %i lines...' % counter
            print 'model is being saved'
            saveModel(model, anomalyLikelihood)
            print 'model saved'

        seq = int(row[0])
        timestamp = int(row[1])
        vx = float(row[2])
        vy = float(row[3])

        #timestamp = datetime.datetime.strptime(row[0], DATE_FORMAT)
        
        result = model.run({
            'seq': seq,
            'timestamp': timestamp,
            'vx': vx,
            'vy': vy
        })

        prediction = result.inferences['multiStepBestPredictions'][1]
        anomalyScore = result.inferences['anomalyScore']
        likelihood = anomalyLikelihood.anomalyProbability(vx, anomalyScore, timestamp)
        loglh = anomalyLikelihood.computeLogLikelihood(likelihood)
        
        writer.writerow((seq, timestamp, vx, vy, prediction, anomalyScore, likelihood, loglh))

    saveModel(model, anomalyLikelihood, seq)
    outf.close()
    inputFile.close()


def saveModel(model, lhmodel):
    model.save(os.path.abspath(MODEL_PATH))
    with open(LH_FILE, 'wb') as lhFile:
        pickle.dump((seq, lhmodel), lhFile)


def runModel(modelParamsName, loadPrevModel=True):
    print 'Creating model from %s...' % modelParamsName

    if loadPrevModel and os.path.exists(MODEL_PATH):
        model = Model.load(MODEL_PATH)
    else:
        model = createModel(getModelParamsFromName(modelParamsName))
    
    inputData = INPUT_DATA
    runIoThroughNupic(inputData, model, modelParamsName, loadPrevModel)


if __name__ == '__main__':
    print 'training is starting, just press Ctrl+C if you want to exit'
    runModel(MODEL_PARAMS_NAME, loadPrevModel=True)
