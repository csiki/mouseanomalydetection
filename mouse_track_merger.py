#!/usr/bin/env python

from os import listdir
from os.path import isfile, join
import json
import pickle
import matplotlib.pyplot as plt
import csv

# merges the generated json files into one csv file, so that Numenta HTM (anomaly detection system) can digest it

mPosPath = "mouse_tracks/"
mPosFiles = [f for f in listdir(mPosPath) if isfile(join(mPosPath, f))]

mPos = []
for mPosFileName in mPosFiles:
    with open(join(mPosPath, mPosFileName)) as mPosFile:
        mPos = mPos + json.loads(mPosFile.read())

# convert to velocity
vs = []
useIndex = 0
for mPosOneTime in mPos:
    timestamp = 0
    for i in xrange(len(mPosOneTime) - 1):
        vs.extend([(useIndex, timestamp, mPosOneTime[i+1][0] - mPosOneTime[i][0], mPosOneTime[i+1][1] - mPosOneTime[i][1])])
        timestamp += 1
    useIndex += 1

#plt.plot(v)
#plt.show()

# min&max
# print 'x', min([v[1] for v in vs]), max([v[1] for v in vs])
# print 'y', min([v[2] for v in vs]), max([v[2] for v in vs])

# save to pickle
# with open('small_v.pickle', 'wb') as merged:
#     pickle.dump(vs, merged)

# write to csv
with open('v.csv', "wt") as merged:
    writer = csv.writer(merged)
    writer.writerow(('seq', 'timestamp', 'vx' ,'vy'))
    writer.writerow(('int', 'int', 'float', 'float'))
    writer.writerow(('S', '', '', ''))
    for v in vs:
        writer.writerow(v)

print "done"
