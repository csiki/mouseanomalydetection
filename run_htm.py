#!/usr/bin/env python

import importlib
import sys
import csv
import datetime
import os, sys
import csv
import zerorpc
import json
import pickle
import subprocess
import signal

from nupic.data.inference_shifter import InferenceShifter
from nupic.frameworks.opf.modelfactory import ModelFactory
from nupic.frameworks.opf.model import Model
from nupic.algorithms import anomaly_likelihood

# runs the anomaly detection model
# waits for mouse tracking inputs coming through rpc, feeds them to the model, and outputs an aggregated anomaly score

DATA_DIR = '.'
MODEL_DIR = 'htm/model'
LH_FILE = 'htm/anomalyLikelihood.pickle'
LOGLH_THRESHOLD = 0.25
MOUSE_TRACK_PROC = None  # mouse tracking nodejs process - terminate when sigint
SERVER = None  # zerorpc server - terminate when sigint
MODEL = None  # HTMSim object

# TODO add option to save model after each movement, default false
# TODO rethink aggregated anomaly score computation
# TODO introduce anomaly threshold parameter learning by running non-saving model while it is known that a hijacker's activity is recorded
# TODO better ways of outputting anomalies
# TODO eps=10^-5 for likelihood


def sigint_handler(signal, frame):
    if MODEL is not None:
        MODEL.saveModel(MODEL_DIR)
        MODEL = None
    if MOUSE_TRACK_PROC is not None:
        MOUSE_TRACK_PROC.terminate()
        MOUSE_TRACK_PROC = None
    if SERVER is not None:
        SERVER.close()
        SERVER = None

    print(' Server and mouse tracking properly terminated, learning model saved')
    sys.exit(0)


class HTMSim(object):

    def __init__(self, modelDir, lhFileName):
        
        self.seq = 0

        print 'Loading model from %s' % modelDir
        self.model = Model.load(modelDir)
        if os.path.isfile(lhFileName):
            with open(lhFileName, 'rb') as lhFile:
                self.seq, self.anomalyLikelihood = pickle.load(lhFile)
                print 'Loaded likelihood estimator from %s' % lhFileName
        else:
            self.anomalyLikelihood = anomaly_likelihood.AnomalyLikelihood()
            print 'Created new likelihood estimator'

    def runModel(self, movesJson):
        moves = json.loads(movesJson)

        # calc velocity
        vs = [ (moves[i+1][0] - moves[i][0], moves[i+1][1] - moves[i][1]) for i in xrange(len(moves)-1) ]

        # run model
        anomalious = False
        anomCount = 0
        for timestamp, v in enumerate(vs):

            vx = v[0]
            vy = v[1]
            result = self.model.run({
                'seq': self.seq,
                'timestamp': timestamp,
                'vx': vx,
                'vy': vy
            })

            # acquire results
            prediction = result.inferences['multiStepBestPredictions'][1]
            anomalyScore = result.inferences['anomalyScore']
            likelihood = self.anomalyLikelihood.anomalyProbability(vx, anomalyScore, timestamp)
            loglh = self.anomalyLikelihood.computeLogLikelihood(likelihood)

            if loglh > LOGLH_THRESHOLD:
                anomCount += 1
        
        self.seq += 1
        if anomCount > len(vs) * 0.1:
            print 'anomaly %f' % (float(anomCount) / len(vs))
        elif anomCount == 0:
            print 'cool    %f' % (float(anomCount) / len(vs))
        else:
            print 'ok      %f' % (float(anomCount) / len(vs))

        return 'gotcha'

    def saveModel(self, modelDir):
        self.model.save(modelDir)
        with open(modelDir + '/' + LH_FILE, 'wb') as lhFile:
            pickle.dump((self.seq, self.anomalyLikelihood), lhFile)


if __name__ == '__main__':
    MODEL = HTMSim(MODEL_DIR, LH_FILE)
    SERVER = zerorpc.Server(MODEL)
    SERVER.bind('tcp://0.0.0.0:4242')
    signal.signal(signal.SIGINT, sigint_handler)
    
    try:
        print 'Mouse tracking starts now'
        MOUSE_TRACK_PROC = subprocess.Popen(['node', 'feed_htm.js'])

        print 'Server starts now'
        print 'Press Ctrl+C to terminate the server and mouse tracking'
        SERVER.run()
    except:
        MOUSE_TRACK_PROC.terminate()
