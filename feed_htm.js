// feeds the mouse activity to the HTM anomaly detection system through an rpc socket
// I could not find a cross platform simple solution for python mouse tracking
// that is why node is used for mouse tracking, but anomaly detection runs in python

// loads
var mouse = require('osx-mouse')();
var zerorpc = require("zerorpc");

// params
var separateMoveThreshold = 2000;

// vars
var lastMove = 0;
var moves = [];
var client = new zerorpc.Client({"heartbeatInterval": 120000, "timeout": 60});

client.connect("tcp://127.0.0.1:4242");

mouse.on('move', function(x, y) {
	checkOnMouse();
    lastMove = new Date().getTime();
    moves.push([x,y]);
});

function checkOnMouse() {
    if (new Date().getTime() - lastMove > separateMoveThreshold) {
    	if (moves.length > 10) {
    		sendToHTM(moves);
            console.log("movements sent");
    	}
    	moves = [];
    }
}
setInterval(checkOnMouse, 100);

function sendToHTM(moves) {
    client.invoke("runModel", JSON.stringify(moves), function(error, res, more) {
        if (res != "gotcha")
            console.log("No HTM response");
    });
}