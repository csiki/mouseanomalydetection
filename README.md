# Mouse Usage Anomaly Detection

Quick and easy solution to detect if your computer (cursor) is hijacked. In short: by training it to learn your own mouse usage habits in the background, it can detect anomalies in the usage - either someone else's cursor movement patterns or that you do something crazy with your mouse.

Uses [NodeJS](https://nodejs.org/en/) modules and the [NuPIC](https://github.com/numenta/nupic) machine intelligence platform for anomaly detection. Check out NuPIC, it's reeeealy cool. Here's a [whitepaper](https://arxiv.org/pdf/1607.02480v1.pdf).
Tested on OS X 10.11.6, but should work on Linux distributions as well.

`dontfaround` part is just an example how you may treat the hijackers: a script (works only on OS X) takes a webcam pics, adds a caption to the picture, saves and opens it for view to the hijacker, while playing a sound from Alien.

## Installation

First, install [NodeJS](https://nodejs.org/en/download/package-manager/). Then install the following packages:

* [osx-mouse](https://www.npmjs.com/package/osx-mouse) for mouse usage tracking,
* [zerorpc](https://www.npmjs.com/package/zerorpc) for communication between the mouse tracking process and the learning process (both run locally),
* [jsonfile](https://www.npmjs.com/package/jsonfile) for storing and loading json files,
* [play-sound]() only for the `dontfaround` (OS X),
* [caption](https://www.npmjs.com/package/caption) only for the `dontfaround` (OS X).

Either use the `--global` keyword to install packages globally, or install them locally wherever and link them to this cloned project.

```
npm install osx-mouse zerorpc jsonfile --global
```

```
npm install play-sound caption --global
```

Install Python packages: `nupic`, `zerorpc`, `numpy` (only for `anal_htm.py`), `matplotlib` (only for `anal_htm.py`).

```
pip install nupic zerorpc numpy matplotlib
```

Finally, clone this repo.

```
git clone https://csiki@bitbucket.org/csiki/mouseanomalydetection.git
```

## Usage

There are three phases, at max, to run mouse usage anomaly detection.

1. Swarming, to find the parameters of the learning algo particularly for mouse movement anomaly detection.

    It takes some time, I've already done this for my Mac Book Pro, and stored the results in `/swarm/model_params/small_v_model_params.py` (it's called `small`, because I used a subset of my 3 week mouse usage, so it ran in ok time, it's `v`, because, actually the system learns the velocity of mouse movements, and not mouse positions).

    You can most likely skip this part, but if you're interested in NuPIC, go on, tweak and run `/swarm/swarm.py`.

2. Training.

    This part has to be done anyway, as only after enough training, NuPIC is able to tell your movement from someone else's.

    One option is to run `track_and_store.js` to... track and store your mouse usage in json files for later training by running `train_htm.py`.

    ```
    node track_and_store.js
    ```

    You can also skip this part, and just run the model to make it learn on the fly like..

3. Run the model.

    Just run `run_htm.py` that learns and outputs the aggregated anomaly score of your mouse movements. Edit this script, to run `dontfaround`, or your own script to react when your mouse is hijacked.
    Beware, the anomaly scores will be high in the first couple of days, depending on the variance of your mouse usage. Give it time.

# Other

If you have any problems, questions, you can reach me on `bengal7ion@gmail.com`.

Contributions are more than welcome. Hope this helps you have some fun - leave your computer open, and wait until someone jumps on the bait :D

