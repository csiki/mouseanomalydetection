# vx and vy min and max values are set according to an experiment (max and min speed of crazy mouse movements)
# on a mac book pro with a apple mouse - conduct your own experiment for optimal swarming parameters

SWARM_DESCRIPTION = {
  "includedFields": [
    {
      "fieldName": "seq",
      "fieldType": "int",
    },
    {
      "fieldName": "timestamp",
      "fieldType": "int",
    },
    {
      "fieldName": "vx",
      "fieldType": "float",
      "maxValue": 980.8,
      "minValue": -1109.8
    },
    {
      "fieldName": "vy",
      "fieldType": "float",
      "maxValue": 250.0,
      "minValue": -273.55
    }
  ],
  "streamDef": {
    "info": "mouse_tracking",
    "version": 1,
    "streams": [
      {
        "info": "Mouse Tracking Data",
        "source": "file://../small_v.csv",
        "columns": [
          "*"
        ]
      }
    ]
  },

  "inferenceType": "TemporalAnomaly",
  "inferenceArgs": {
    "predictionSteps": [
      1
    ],
    "predictedField": "vx"  # try first single predicted field swarming
  },
  "iterationCount": -1,
  "swarmSize": "medium"  # TODO set to medium
}
