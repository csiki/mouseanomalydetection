#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import numpy as np
import math

# to visualize the anomaly scores and likelihood values outputted by the HTM anomaly detection system


def movingaverage(interval, window_size):
    window = np.ones(int(window_size)) / float(window_size)
    return np.convolve(interval, window, 'same')


seq = []
timestamp = []
vx = []
vy = []
pred = []
score = []
lh = []
loglh = []
with open('small_v_out.csv', 'rt') as htmData:
    reader = csv.reader(htmData)
    reader.next()
    for row in reader:
        seq.append(int(row[0]))
        timestamp.append(int(row[1]))
        vx.append(float(row[2]))
        vy.append(float(row[3]))
        pred.append(float(row[4]))
        score.append(float(row[5]))
        lh.append(float(row[6]))
        loglh.append(float(row[7]))

print 'load done'


# go through loglh>0.5
loglhnp = np.array(loglh)
anomalies = loglhnp > 0.5
a_ma = movingaverage(anomalies, 50000)

plt.plot(a_ma)
plt.title('loglh moving avg')

# plt.plot(score)
# plt.title('score')

# plt.figure()
# plt.plot(lh)
# plt.title('likelihood')

# plt.figure()
# plt.plot(loglh)
# plt.title("loglikelihood")
# plt.xscale('log')

# plt.figure()
# plt.plot(vx)
# plt.plot(pred)

plt.show()
