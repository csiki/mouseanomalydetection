// loads
var mouse = require('osx-mouse')();
const exec = require('child_process').exec;
var player = require('play-sound')(opts = {});
var caption = require('caption');

DFAPATH = '/Users/vtoth/apps/node_modules/osx-mouse/';

// TODO mouse movement learning mode
// TODO https://www.npmjs.com/package/convnet
// https://www.npmjs.com/package/opencv_faced_detect

console.log('I warned you.');

// params
var separateMoveThreshold = 2000;
var buckets = [20]; // max 1000, 50 length, 20 pieces of buckets TODO
var lastTimeITSMEThreshold = 5000;

// vars
var lastMove = 0;
var moves = [];
var lastTimeITSME = new Date().getTime();

mouse.on('move', function(x, y) {
	checkOnMouse();
    lastMove = new Date().getTime();
    moves.push([x,y]);
});

function checkOnMouse() {
    if (new Date().getTime() - lastMove > separateMoveThreshold) {
    	if (moves.length > 10) {
    		analyzeMoves(moves);
    	}
    	moves = [];
    }
}
setInterval(checkOnMouse, 100);

function analyzeMoves(moves) {
	dt = derivative(moves);
    console.log('max', Math.max.apply(null, dt));
    console.log('min', Math.min.apply(null, dt));
    console.log('avg', mean(dt));
    console.log('var', variance(dt));

    var t = new Date().getTime();
    if (t - lastTimeITSME > lastTimeITSMEThreshold) {
    	// TODO check on img or mouse patterns
    	dontFuckAround();
    } else {
    	lastTimeITSME = t;
    	console.log('itsme');
    }
}

function analyzeImg() {
	// TODO set ITSME
}

function mean(arr) {
	var avg = arr.reduce((a, b) => a + b, 0);
	return avg / arr.length;
}

function variance(arr) {
	var avg = mean(arr);
	var v = 0;
	for (var i = 0; i < arr.length; ++i)
		v += Math.pow(arr[i] - avg, 2);
	return v / arr.length;
}

function norm_histogram(arr, buckets) {
	//for ()
}

function derivative(arr) {
	var dt = []
	for (var i = 0; i < arr.length - 1; ++i)
		dt.push(dist(arr[i], arr[i+1]));
	return dt;
}

function dist(a, b) {
	return Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2));
}

//dontFuckAround(); // for testing

function dontFuckAround() {
	// set playback device
	exec('audiodevice output "Display Audio"', (err, stdout, stderr) => {
	  	// set volume
		exec('osascript -e "set Volume 10"', (err, stdout, stderr) => { // TODO set to 10
			// stay on high volume
		  	var volset = setInterval(function() { setVolume(10) }, 500); // TODO set to 10
		  	// capture & caption img
		  	var imgPath = captureImg();
		  	// play sound
		  	player.play(DFAPATH + 'getaway.mp3', function(err){
		  		if (err)
		  			console.log(err);
		  		console.log('played sound');
		  		clearInterval(volset);
		  		setPlaybackDevice('Headphones');
		  	});
		});
	});
}

function captureImg() {
	var d = new Date()
	var t = d.getTime();
	imgPath = DFAPATH + 'intruders/' + t + '.png';
	// snap
	exec('imagesnap ' + imgPath, (err, stdout, stderr) => {
	  	if (err) {
	    	console.log(err);
	    	return;
	  	}
	  	console.log('imagesnap');
	  	// caption
	  	caption.path(imgPath, {
	  		caption: 'YOU BETTER RUN',
	  		outputFile: imgPath
		}, function(err, fname) {
			if (err)
				console.log(err);
			console.log('captioned');
			// open
			exec('open ' + imgPath, (err, stdout, stderr) => {
				console.log('opened');
			});
		});
	});
	return imgPath;
}

function setPlaybackDevice(to) {
	exec('audiodevice output "' + to + '"', (err, stdout, stderr) => {
	  if (err) {
	    console.log(err);
	    return;
	  }
	  console.log('playbackdevice set to ' + to);
	});
}

function setVolume(to) {
	exec('osascript -e "set Volume ' + to + '"', (err, stdout, stderr) => {
	  if (err) {
	    console.error(err);
	    return;
	  }
	  console.log('volume set to ' + to);
	});
}
